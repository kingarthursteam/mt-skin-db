--
-- Tabellenstruktur für Tabelle `mt_skins`
--
-- Erzeugt am: 09. Aug 2013 um 23:26
--

CREATE TABLE IF NOT EXISTS `mt_skins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'name of skin',
  `author` varchar(255) NOT NULL,
  `license` varchar(255) NOT NULL,
  `uploaded` datetime NOT NULL COMMENT 'date and time of the upload',
  `type` varchar(255) NOT NULL COMMENT 'mime type',
  `img` text CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;
