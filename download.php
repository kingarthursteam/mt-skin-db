<? 
include("connectsql.php");

function filenameReplaceBadChars( $filename ) {
  
  $patterns = array(
    "/\\s/",  # Leerzeichen
    "/\\&/",  # Kaufmaennisches UND
    "/\\+/",  # Plus-Zeichen
    "/\\</",  # < Zeichen
    "/\\>/",  # > Zeichen
    "/\\?/",  # ? Zeichen
    "/\"/",   # " Zeichen
    "/\\:/",  # : Zeichen
    "/\\|/",  # | Zeichen
    "/\\\\/",   # \ Zeichen
    "/\\//",  # / Zeichen
    "/\\*/"   # * Zeichen
  );
  
  $replacements = array(
    "_",
    "-",
    "-",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9"
  );
  
  return preg_replace( $patterns, $replacements, $filename );
   
} 

if (isset($_GET[id]) && is_numeric($_GET[id])) {
	$sql = "SELECT * FROM `mt_skins` WHERE `id` = '$_GET[id]' LIMIT 0 , 1";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);

	
	$bin = base64_decode($row[img]);
	
	$img =  imagecreatefromstring($bin);
	imagesavealpha($img,true);
	header("Content-Type: application/force-download");
	$name = filenameReplaceBadChars($row[name]);
	$author = filenameReplaceBadChars($row[author]);
	header("Content-Disposition: attachment; filename=". $name."_by_".$author.".png");
	
	imagepng ($img,NULL);

} 
else {
	echo "missing or invalid param 'id'";
}
?>