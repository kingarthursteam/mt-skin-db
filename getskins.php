<?php
include ("connectsql.php");

if ($_POST[filtertype] == 'Author' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins`.`author` LIKE '%" . mysql_real_escape_string($_POST[filter]) . "%'";
} else if ($_POST[filtertype] == 'License' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins_licenses`.name LIKE '%" . mysql_real_escape_string($_POST[filter]) . "%'";
} else if ($_POST[filtertype] == 'Id' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins`.`id` = '" . mysql_real_escape_string($_POST[filter]) . "'";
} else if ($_POST[filtertype] == 'Name' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins`.`name` LIKE '%" . mysql_real_escape_string($_POST[filter]) . "%'";
} else {
	$sql = "";
}
$filterarr = array();

if ($_POST[name] and $_POST[name] != ""){
	$filterarr[] = "`mt_skins`.`name` LIKE '%" . mysql_real_escape_string($_POST[name]) . "%'";
}
if ($_POST[author] and $_POST[author] != ""){
	$filterarr[] = "`mt_skins`.`author` LIKE '%" . mysql_real_escape_string($_POST[author]) . "%'";
}
if ($_POST[license] and $_POST[license] != ""){
	$filterarr[] = "`mt_skins`.`license` LIKE '%" . mysql_real_escape_string($_POST[license]) . "%'";
}
if ($_POST[id] and $_POST[id] != ""){
	$ids = explode(",",$_POST['id']);//todo: check if here are some security leaks.
	$filterarr[] = "`mt_skins`.`id` IN ('" . implode("','", $ids) . "')";
}
if (count($filterarr)>=1){
	$sql = "WHERE " . implode(" AND ", $filterarr);
}
else $sql = "";

$count = "SELECT count(*) FROM `mt_skins` LEFT JOIN `mt_skins_licenses` ON `mt_skins_licenses`.id = `mt_skins`.license " . $sql;

$result = mysql_query($count) OR die("<pre>\n" . $count . "</pre>\n" . mysql_error());
$anzahl = (int)mysql_result($result, 0);
$output[count_results] = $anzahl;

if ($anzahl < 1) {
	$output[page] = 0;
	$output[pages] = 0;
	$output[debug] = $count;
	$output[success] = false;
	$output[status_msg] = "ERROR: No skin found matching to your Filter\n please try another.";
	
} else {

	// Festlegen der aktuellen Seite
	$start = isset($_POST['page']) ? (int)$_POST['page'] : 1;
	// Festlegen der Anzahl der angezeigten Datensätze
	$per_page = isset($_POST['per_page']) ? (int)$_POST['per_page'] : 20;
	if ($per_page != 5 AND $per_page != 10 AND $per_page != 20)
		$per_page = 10;
	// Berechnung der Seitenzahlen = Alle Datensätze geteilt durch Datensätze pro Seite
	$num_pages = ceil($anzahl / $per_page);

	// Überprüft, ob eine mögliche Seitenzahl übergeben wurde
	if ($start < 1)
		$start = 1;
	if ($start > $num_pages)
		$start = $num_pages;

	$offset = ($start - 1) * $per_page;
	$output[page] = $start;
	$output[pages] = $num_pages;
	$output[per_page] = $per_page;
	$output[success] = true; 
	
$sql = <<<sql
	 SELECT 
	 `mt_skins`.id, 
	 `mt_skins`.name, 
	 `mt_skins`.author, 
	 `mt_skins`.uploaded, 
	 `mt_skins`.type, 
	 `mt_skins`.img, 
	 `mt_skins`.cape_compatible,
	 mt_skins_licenses.name AS license,
	 mt_skins_licenses.id AS license_id
	FROM `mt_skins`
	LEFT JOIN `mt_skins_licenses` ON `mt_skins_licenses`.id = `mt_skins`.license
	 $sql LIMIT $offset,$per_page; 
sql;
	$output[debug] = $sql;
	$result = mysql_query($sql);
	if ($result == false) {
		$output[success] = false;
		$output[status_msg] = 'ERROR: could not load Skin. Error in Mysql. MYSQL says: ' . mysql_error();
		//$output[debug] = $per_page;
	} else {
		$output[data] = Array();
		while ($row = mysql_fetch_assoc($result)) {
			$output[data][] = $row;
			$output[success]= true;
		}
	}
}

header('Content-Type: application/json');
echo json_encode($output);
?>