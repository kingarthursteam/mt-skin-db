<?php
function ImageCopyFliped($dst_im,$src_im,
							$dst_x,$dst_y,
							$src_x,$src_y,
							$src_w,$src_h ){
$img_temp =  imagecreatetruecolor($src_w, $src_h);
imagesavealpha($img_temp,true);
	    $white = imagecolorallocatealpha($img_temp, 255, 255, 255, 127);
    imagefill($img_temp, 0, 0, $white);

imageCopy($img_temp, $src_im, //linker arm Spiegeln!!
               0, 0,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               $src_x, $src_y,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               $src_w,$src_h);      /* und ist 96 Pixel breit und 90 Pixel hoch */
               
               
               imageflip($img_temp,IMG_FLIP_HORIZONTAL);
			   
imageCopy($dst_im, $img_temp, //linker arm Spiegeln!!
               $dst_x,$dst_y,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               0, 0,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               $src_w,$src_h);      /* und ist 96 Pixel breit und 90 Pixel hoch */
	
imagedestroy($img_temp)	;
}

function create2d($img_src,$scale){
	imagesavealpha($img_src,true);
	$width = 16;
	$height = 32;
	$img_out =  imagecreatetruecolor($width, $height);
	    $white = imagecolorallocatealpha($img_out, 255, 255, 255, 127);
    imagefill($img_out, 0, 0, $white);
	imagesavealpha($img_out,true);


imageCopy($img_out, $img_src, //kopf
               4, 0,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               8, 8,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               8, 8);      /* und ist 96 Pixel breit und 90 Pixel hoch */

imageCopy($img_out, $img_src, //rechter arm
               0, 8,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               44, 20,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               4, 12);      /* und ist 96 Pixel breit und 90 Pixel hoch */

imageCopyFliped($img_out, $img_src, //linker arm Spiegeln!!
               12, 8,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               44, 20,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               4, 12);      /* und ist 96 Pixel breit und 90 Pixel hoch */
              
imageCopy($img_out, $img_src, //bauch
               4, 8,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               20, 20,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               8, 12);      /* und ist 96 Pixel breit und 90 Pixel hoch */

imageCopy($img_out, $img_src, //rechtes bein
               8, 20,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               4, 20,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               4, 12);      /* und ist 96 Pixel breit und 90 Pixel hoch */
               
imageCopyFliped($img_out, $img_src, //linkes bein Spiegeln!!
               4, 20,     /* imagecopy() an die Stelle ( 202, 0) in $Wiese, */
               4, 20,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
               4, 12);      /* und ist 96 Pixel breit und 90 Pixel hoch */
               
    //imageantialias($img_out, false);
    $width = 16;
	$height = 32;
	$newwidth = $width * $scale;
	$newheight = $height * $scale;
	$out = imagecreatetruecolor($newwidth, $newheight);
	$white = imagecolorallocatealpha($out, 255, 255, 255, 127);
    imagefill($out, 0, 0, $white);
	//imagesavealpha($img_out,true);  
	//imagesavealpha($thumb,true);  
	imagecopyresized($out, $img_out, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    imagesavealpha($out,true);
	return $out;
	} 
function save_images($img_src,$id){
	$size = 1;
	$thumb = create2d($img_src,$size);
               	//header("Content-Type: image/png");
               	@mkdir("./skins");
				@mkdir("./skins/".$size."");
	imagepng($thumb,"./skins/".$size."/".$id.".png");
	
	$size = 5;
	$thumb = create2d($img_src,$size);
               	//header("Content-Type: image/png");
               	@mkdir("./skins");
				@mkdir("./skins/".$size."");
	imagepng($thumb,"./skins/".$size."/".$id.".png");
	return true;
}

?>