<?php
include ("../connectsql.php");

function filenameReplaceBadChars($filename) {

	$patterns = array("/\\s/", # Leerzeichen
	"/\\&/", # Kaufmaennisches UND
	"/\\+/", # Plus-Zeichen
	"/\\</", # < Zeichen
	"/\\>/", # > Zeichen
	"/\\?/", # ? Zeichen
	"/\"/", # " Zeichen
	"/\\:/", # : Zeichen
	"/\\|/", # | Zeichen
	"/\\\\/", # \ Zeichen
	"/\\//", # / Zeichen
	"/\\*/" # * Zeichen
	);

	$replacements = array("_", "-", "-", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	return preg_replace($patterns, $replacements, $filename);

}

function apiDokumentation() {
	$out = <<<OUT
    	asdf
OUT;
	return $out;
}

if (isset($_GET[apidoku])) {
	echo apiDokumentation();
} elseif (isset($_GET[getsingle], $_GET[outformat]) && (isset($_GET[id]) && is_numeric($_GET[id])) || isset($_GET[playername], $_GET[pwd])) {
	$sql = "SELECT * FROM `mt_skins` WHERE `id` = '$_GET[id]' LIMIT 0 , 1";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	$num = mysql_num_rows($result);
	if ($num < 1) {
		$out[success] = false;
		$out[errmsg] = "missing or unknown param outformat";
	}

	if ($_GET[outformat] == 'bin') {
		$bin = base64_decode($row[img]);

		$img = imagecreatefromstring($bin);
		imagesavealpha($img, true);
		header("Content-Type: " . $row[type]);
		$name = filenameReplaceBadChars($row[name]);
		$author = filenameReplaceBadChars($row[author]);
		header("Content-Disposition: attachment; filename=" . $name . "_by_" . $author . ".png");

		imagepng($img, NULL);
	} elseif ($_GET[outformat] == 'base64') {

		$out = $row;
		$out[success] = true;

	} elseif ($_GET[outformat] == 'hex') {
		$bin = base64_decode($row[img]);
		$hex = bin2hex($bin);
		$out = $row;
		$out[img] = $hex;
		$out[success] = true;

	} else {
		$out[success] = false;
		$out[errmsg] = "missing or unknown param outformat";
	}

} elseif (isset($_GET[getlist], $_GET[page])) {

	$sql = "";
	$count = "SELECT count(*) FROM `mt_skins` " . $sql;

	$result = mysql_query($count) OR die("<pre>\n" . $count . "</pre>\n" . mysql_error());
	$anzahl = mysql_result($result, 0);

	if (!$anzahl) {
		$out[page] = 1;
		$out[pages] = 1;
	} else {

		// Festlegen der aktuellen Seite
		$start = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		// Festlegen der Anzahl der angezeigten Datensätze
		$per_page = isset($_GET['per_page']) ? (int)$_GET['per_page'] : 10;
		if ($per_page < 1 AND $per_page > 100)
			$per_page = 10;
		// Berechnung der Seitenzahlen = Alle Datensätze geteilt durch Datensätze pro Seite
		$num_pages = ceil($anzahl / $per_page);

		// Überprüft, ob eine mögliche Seitenzahl übergeben wurde
		if ($start < 1)
			$start = 1;
		if ($start > $num_pages)
			$start = $num_pages;

		$offset = ($start - 1) * $per_page;
		$out[page] = $start;
		$out[pages] = $num_pages;
		$out[per_page] = $per_page;
	}

$sql = <<<SQL
select
	`mt_skins`.id, 
	`mt_skins`.name, 
	`mt_skins`.author, 
	`mt_skins`.uploaded, 
	`mt_skins`.type, 
	`mt_skins_licenses`.`name` AS license,
	`mt_skins_licenses`.`id` AS license_id,
	GROUP_CONCAT(`mt_tag`.`description` SEPARATOR ', ')AS tags,
	`mt_skins`.img
from `mt_skins` 
left JOIN (`mt_skins_licenses`)
ON (`mt_skins_licenses`.`id` = `mt_skins`.`license`
) 
left JOIN (`mt_skins_tag_map`,`mt_tag`)
ON(`mt_skins_tag_map`.`mt_skins_id` = `mt_skins`.id 
    AND `mt_skins_tag_map`.mt_tag_id = `mt_tag`.tag_id)
GROUP BY `mt_skins`.id
	 LIMIT $offset,$per_page;
SQL;
	//echo $sql;
	$result = mysql_query($sql);
	if ($result == false) {
		$out[success] = false;
		$out[errmsg] = 'ERROR: could not load Skin. Error in Mysql. MYSQL says: ' . mysql_error();
	} else {
		while ($row = mysql_fetch_assoc($result)) {
			if (isset($_GET[outformat]) && $_GET[outformat] == "hex") {
				$bin = base64_decode($row[img]);
				$hex = bin2hex($bin);
				$row[img] = $hex;
			}
			$row[tags] = explode(", ", $row[tags]);
			$out[skins][] = $row;
		}
		$out[success] = true;
	}
} elseif (isset($_GET[getskinbyplayer])) {
	echo apiDokumentation();
} elseif (isset($_GET[licenses])) {
	
$sql = <<<SQL
SELECT *
FROM `mt_skins_licenses`
SQL;
	$result = mysql_query($sql);
	if ($result == false) {
		$out[success] = false;
		$out[errmsg] = 'ERROR: could not load licenses. Error in Mysql. MYSQL says: ' . mysql_error();
	} else {
		while ($row = mysql_fetch_assoc($result)) {
			$row[type] = "text";
			if($row[url] != null) {				
				$row[type] = "url";
			}

			$out[licenses][] = $row;
		}
		$out[success] = true;
	}
} else {
	echo apiDokumentation();
}
if ($out) {
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
	$strout = json_encode($out, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
	$length = strlen($strout);
    header('Content-Length: '.$length);
	echo $strout;
}
?>