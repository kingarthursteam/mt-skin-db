<?php
include ("../connectsql.php");
if ($_POST[filtertype] == 'Author' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins`.`author` LIKE '%" . mysql_real_escape_string($_POST[filter]) . "%'";
} else if ($_POST[filtertype] == 'License' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins_licenses`.name LIKE '%" . mysql_real_escape_string($_POST[filter]) . "%'";
} else if ($_POST[filtertype] == 'Id' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins`.`id` = '" . mysql_real_escape_string($_POST[filter]) . "'";
} else if ($_POST[filtertype] == 'Name' && isset($_POST[filter])) {
	$sql = "WHERE `mt_skins`.`name` LIKE '%" . mysql_real_escape_string($_POST[filter]) . "%'";
} else {
	$sql = "";
}
$filterarr = array();

if ($_POST[name] and $_POST[name] != ""){
	$filterarr[] = "`mt_skins`.`name` LIKE '%" . mysql_real_escape_string($_POST[name]) . "%'";
}
if ($_POST[author] and $_POST[author] != ""){
	$filterarr[] = "`mt_skins`.`author` LIKE '%" . mysql_real_escape_string($_POST[author]) . "%'";
}
if ($_POST[license] and $_POST[license] != ""){
	$filterarr[] = "`mt_skins_licenses`.`name` LIKE '%" . mysql_real_escape_string($_POST[license]) . "%'";
}
if ($_POST[id] and $_POST[id] != ""){
	$ids = explode(",",$_POST['id']);//todo: check if here are some security leaks.
	$filterarr[] = "`mt_skins`.`id` IN ('" . implode("','", $ids) . "')";
}
if (count($filterarr)>=1){
	$sql = "WHERE " . implode(" AND ", $filterarr);
}
else $sql = "";
function filenameReplaceBadChars($filename) {

	$patterns = array("/\\s/", # Leerzeichen
	"/\\&/", # Kaufmaennisches UND
	"/\\+/", # Plus-Zeichen
	"/\\</", # < Zeichen
	"/\\>/", # > Zeichen
	"/\\?/", # ? Zeichen
	"/\"/", # " Zeichen
	"/\\:/", # : Zeichen
	"/\\|/", # | Zeichen
	"/\\\\/", # \ Zeichen
	"/\\//", # / Zeichen
	"/\\*/" # * Zeichen
	);

	$replacements = array("_", "-", "-", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	return preg_replace($patterns, $replacements, $filename);

}

function apiDokumentation() {
	$out = <<<OUT
    	asdf
OUT;
	return $out;
}

if (isset($_POST[apidoku])) {
	echo apiDokumentation();
} elseif (isset($_POST[getsingle], $_POST[outformat]) && (isset($_POST[id]) && is_numeric($_POST[id])) || isset($_POST[playername], $_POST[pwd])) {
	$sql = "SELECT * FROM `mt_skins` WHERE `id` = '$_POST[id]' LIMIT 0 , 1";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	$num = mysql_num_rows($result);
	if ($num < 1) {
		$out[success] = false;
		$out[errmsg] = "missing or unknown param outformat";
	}

	if ($_POST[outformat] == 'bin') {
		$bin = base64_decode($row[img]);

		$img = imagecreatefromstring($bin);
		imagesavealpha($img, true);
		header("Content-Type: " . $row[type]);
		$name = filenameReplaceBadChars($row[name]);
		$author = filenameReplaceBadChars($row[author]);
		header("Content-Disposition: attachment; filename=" . $name . "_by_" . $author . ".png");

		imagepng($img, NULL);
	} elseif ($_POST[outformat] == 'base64') {

		$out = $row;
		$out[success] = true;

	} elseif ($_POST[outformat] == 'hex') {
		$bin = base64_decode($row[img]);
		$hex = bin2hex($bin);
		$out = $row;
		$out[img] = $hex;
		$out[success] = true;

	} else {
		$out[success] = false;
		$out[errmsg] = "missing or unknown param outformat";
	}

} elseif (isset($_POST[getlist], $_POST[page])) {

	$count = "SELECT count(*) FROM `mt_skins` LEFT JOIN `mt_skins_licenses` ON `mt_skins_licenses`.id = `mt_skins`.license " . $sql;

	$result = mysql_query($count) OR die("<pre>\n" . $count . "</pre>\n" . mysql_error());
	$anzahl = mysql_result($result, 0);

	if ($anzahl < 1) {
	$out[page] = 0;
	$out[pages] = 0;
	$out[debug] = $count;
	$out[success] = false;
	$out[status_msg] = "ERROR: No skin found matching to your Filter\n please try another.";
	
} else {

	// Festlegen der aktuellen Seite
	$start = isset($_POST['page']) ? (int)$_POST['page'] : 1;
	// Festlegen der Anzahl der angezeigten Datensätze
	$per_page = isset($_POST['per_page']) ? (int)$_POST['per_page'] : 20;
//	if ($per_page != 5 AND $per_page != 10 AND $per_page != 20)
//		$per_page = 10;
	// Berechnung der Seitenzahlen = Alle Datensätze geteilt durch Datensätze pro Seite
	$num_pages = ceil($anzahl / $per_page);

	// Überprüft, ob eine mögliche Seitenzahl übergeben wurde
	if ($start < 1)
		$start = 1;
	if ($start > $num_pages)
		$start = $num_pages;

	$offset = ($start - 1) * $per_page;
	$out[page] = $start;
	$out[pages] = $num_pages;
	$out[per_page] = $per_page;
	$out[success] = true; 
	
$sql = <<<sql
select 
`mt_skins`.`id` AS `id`,
`mt_skins`.`name` AS `name`,
`mt_skins`.`author` AS `author`,
`mt_skins`.`uploaded` AS `uploaded`,
`mt_skins`.`type` AS `type`,
`mt_skins`.`img` AS `img`,
`mt_skins`.`cape_compatible` AS `cape_compatible`,
`mt_skins_licenses`.`name` AS `license`,
`mt_skins_licenses`.`id` AS `license_id`,
group_concat(`mt_tag`.`description` separator ', ') AS `tags`

from (`mt_skins` 
       left join (`mt_skins_tag_map` 
                  join `mt_tag`) on(`mt_skins_tag_map`.`mt_skins_id` = `mt_skins`.`id`) and (`mt_skins_tag_map`.`mt_tag_id` = `mt_tag`.`tag_id`)) 
JOIN `mt_skins_licenses` ON `mt_skins_licenses`.id = `mt_skins`.license

	 $sql 
	 group by `mt_skins`.`id`
	 LIMIT $offset,$per_page; 
sql;
	$out[debug] = $sql;
	$result = mysql_query($sql);
	if ($result == false) {
		$out[success] = false;
		$out[errmsg] = 'ERROR: could not load Skin. Error in Mysql. MYSQL says: ' . mysql_error();
		$out[sql] = $sql;
	} else {
		$output[data] = Array();
		while ($row = mysql_fetch_assoc($result)) {
			$row['tags'] = preg_split('@, @', $row['tags'], NULL, PREG_SPLIT_NO_EMPTY);
			$out[skins][] = $row;
			$out[success]= true;
		}
	}
	}}
if ($out) {
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
	$strout = json_encode($out, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
	$length = strlen($strout);
    header('Content-Length: '.$length);
	echo $strout;
}
?>