Api Dokumentation for the open Skin DB
======================================

currently there are 2 ways aviable to access it.

- http://minetest.fensta.bplaced.net/api/get.json.php
- http://minetest.fensta.bplaced.net/api/post.json.php

as the name says to the first file you must attach the parameter as get
and to the secound file you need to post it.

there are currently the following parameter:

- getsingle (no value)
- outformat (bin , hex, base64)
- id (number)

Output
------

outformat = "hex" and "base64" will return JSON containing the image as "img" string hex or base64 coded
the "bin" output will return the plain image file instead.
look at example 1


example 1: if you want to get the skin #1 as binary
<pre>
http://minetest.fensta.bplaced.net/api/get.json.php?getsingle&outformat=bin&id=1
</pre>
this will return the whole image file named as  

    <name>_by_<author>.png

example 2: get the skin as base64 as JSON
<pre>
http://minetest.fensta.bplaced.net/api/get.json.php?getsingle&outformat=base64&id=1
</pre>

this will return the skin and some information.

    {
       "id": "1",
       "name": "Sam 0",
       "author": "Jordach",
       "license": "CC BY-SA 3.0",
       "uploaded": "0000-00-00 00:00:00",
       "type": "image\/png",
       "img": "<a realy long base64 string>",
       "success": true
    }

the JSON response
-----------------

if the request is successfull it will return a JSON wich contains success = true;
otherwise the success will be false and the error in the errormsg;
a successfull JSON response will look like this

    {
       "id": <id of the skin>(number),
       "name": <the name of the skin>(string),
       "author": <the name of the author>(string),
       "license": <the license>(string),
       "uploaded": <the upload date time> (string datetime YYY-MM-DD HH:MM:SS),
       "type": <the mime type of the image>(string),
       "img": <a base64 or hex string of the binary data>,
       "success": <the state of the response>(bool)
    }


getlist
-------
the JSON response for the getlist
<pre>/api/get.json.php?getlist&outformat=base64&page=1</pre>
this will return a JSON containing an array "skin" wich contains 10 single skins.

###Param's###

*per_page*:
the amount of the skins can be set with the param *per_page*. possible values for per_page are 5,10,20 the default is 10.


*page*: 
the page is just the number of wich page wich shuld be read.

outformat:
the outformat of the binary files. possible values are base64 or hex

###example output:###

    http://minetest.fensta.bplaced.net/api/get.json.php?getlist&page=1&outformat=base64

    {
    "page": 1,
    "pages": 3,
    "per_page": 10,
    "skins": [
        {
            "id": 1,
            "name": "Sam 0",
            "author": "Jordach",
            "license": "CC BY-SA 3.0",
            "license_id":1,
            "uploaded": "0000-00-00 00:00:00",
            "type": "image/png",
            "img": "iVBOEUgAAAEAAAA.....Bhc3NpdmUvSHVtYW7b7F0Y"
        },
        {
            "id": 2,
            "name": "Sam I",
            "author": "Jordach",
            "license": "CC BY-SA 3.0",
            "license_id":1,
            "uploaded": "0000-00-00 00:00:00",
            "type": "image\/png",
            "img": "iVBORw0KGgoA.....WhQTz\/1+E5ErkJggg=="
        },
        {
        ...
        },
        ...
        ],
    "success"=true
    }




the json format:
----------------

the json format is a very strong data-interchange format wich can be readen and written by very much programming languages.

only lua dose it not know by default. but there exist a function to transform it into a luatable. <http://regex.info/blog/lua/json>

base64 or hex?:
---------------

base64 is a format for binary files wich needs less space than the hex format.

for one skin its 0,2 KB less than hex.
for the whole skin list its 2 KB lesser.
to convert a base64 string into a binary string you can show at this page:
<http://lua-users.org/wiki/BaseSixtyFour>.

